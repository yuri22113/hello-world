import java.util.Scanner;

public class HelloWorld3 {
    static void order(String food) {
        System.out.println("You have ordered "+food+".Thank you!");
    }

    public static void main(String[] args) {
        System.out.println("What would you like to order");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");
        System.out.println("Your order[1-3]");
        Scanner userIn = new Scanner(System.in);
        int num = userIn.nextInt();
        userIn.close();

        if(num==1){
            order("Tempura");
        }
        if(num==2){
            order("Ramen");
        }
        if(num==3){
            order("Udon");
        }

    }
}
