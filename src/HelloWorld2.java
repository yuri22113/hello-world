import java.util.Scanner;

public class HelloWorld2 {
    public static void main(String[] args) {
        System.out.println("What would you like to order");
        System.out.println("1.Tempura");
        System.out.println("2.Ramen");
        System.out.println("3.Udon");

        Scanner userIn = new Scanner(System.in);
        int num = userIn.nextInt();
        userIn.close();

        if(num==1) {
            System.out.println("you have ordered Tempura Thank you!");
        }
        else if(num==2){
            System.out.println("you have ordered Ramen Thank you!");
        }
        else if(num==3){
            System.out.println("you have ordered Udon Thank you!");
        }
    }
}
