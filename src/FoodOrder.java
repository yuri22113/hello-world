import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodOrder {
    private JPanel root;
    private JLabel topLabel;
    private JButton ramenButton;
    private JButton sobaButton;
    private JButton takoyakiButton;
    private JButton sushiButton;
    private JButton udonButton;
    private JButton steakButton;
    private JButton payButton;
    private JTextPane orderedbox;
    private JTextPane pricesum;
    private JButton largeButton;
    private JLabel list;
    private JLabel total;
    private JButton forhere;
    private JButton togo;

    int sum=0;
    double tax=0;
    double intax=0;

    void order(String food,int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order "+food+ "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null,"Order for "+food+" received.");
            String currentText = orderedbox.getText();
            orderedbox.setText(currentText +food+"  "+price+" yen\n");
            sum+=price;
            pricesum.setText(sum+" yen\n");
        }
    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodOrder");
        frame.setContentPane(new FoodOrder().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public FoodOrder() {
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("ramen.jpg")
        ));
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",500);
            }
        });

        sobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("soba.jpg")
        ));
        sobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Soba",400);
            }
        });

        udonButton.setIcon(new ImageIcon(
                this.getClass().getResource("udon.jpg")
        ));
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon",350);
            }
        });

        takoyakiButton.setIcon(new ImageIcon(
                this.getClass().getResource("takoyaki.jpg")
        ));
        takoyakiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Takoyaki",200);
            }
        });

        sushiButton.setIcon(new ImageIcon(
                this.getClass().getResource("sushi.jpg")
        ));
        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi",700);
            }
        });

        steakButton.setIcon(new ImageIcon(
                this.getClass().getResource("steak.jpg")
        ));
        steakButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Steak",800);
            }
        });

        largeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String currentText = orderedbox.getText();
                orderedbox.setText(currentText +"+100 yen\n");
                sum+=100;
                pricesum.setText(sum+" yen\n");
            }
        });

        payButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Would you like to finish order and pay the bill?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null,"Thank you.See you again soon.");
                    orderedbox.setText("");
                    sum=0;
                    pricesum.setText(sum+" yen\n");
                }

            }
        });
        forhere.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tax=sum*0.1;
                intax=sum+tax;
                pricesum.setText(sum+" yen\nTax:     "+(int)tax+" yen\nTotal: "+(int)intax+" yen\n");

            }
        });
        togo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tax=sum*0.08;
                intax=sum+tax;
                pricesum.setText(sum+" yen\nTax:     "+(int)tax+" yen\nTotal: "+(int)intax+" yen\n");

            }
        });
    }
}
